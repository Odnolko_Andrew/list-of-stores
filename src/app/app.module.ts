import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { ShopsComponent } from './components/shops/shops.component';
import { ShopComponent } from './components/shops/shop/shop.component';
import { ShopsService } from './shared/service/shops.service';
import { StorageService } from './shared/service/storage.service';
import { GmapsComponent } from './components/gmaps/gmaps.component';
import { ProductsModule } from './components/shops/shop/products/products.module';



@NgModule({
  declarations: [
    AppComponent,
    ShopsComponent,
    GmapsComponent,
    ShopComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ProductsModule
  ],
  providers: [StorageService, ShopsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
