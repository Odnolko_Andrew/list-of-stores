import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Location } from '@angular/common';
import { ProductModel } from 'src/app/shared/models/product.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.less']
})
export class ProductsComponent implements OnInit {
  listProduct: any;
  url: string;
  title: string;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      if (!params.products) {
        this.router.navigate(['shops']);
        return;
      }
      this.listProduct = JSON.parse(params.products).map((el) => {
        return new ProductModel(el.id, el.title, el.price, el.description);
      });
    });
    this.location.replaceState(this.location.path().split(';')[0], '');
  }

  goToProduct(event) {
    this.title = event.currentTarget.innerHTML;
  }
}

