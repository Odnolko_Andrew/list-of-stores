import { Component, OnInit, Input, Output } from '@angular/core';
import {Shop} from '../../../shared/models/shop.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.less']
})
export class ShopComponent implements OnInit {
  @Input() store: Shop;
  products: string;

  isAddress: boolean;
  isShedule: boolean;

  stores: Shop[];
  shop1: Shop;
  shop2: Shop;
  shop3: Shop;
  shop4: Shop;

  constructor(private router: Router) {}

  ngOnInit() {}

  goToProductDetails() {
    this.products = JSON.stringify(this.store.products);
    this.router.navigate(['/products', {products: this.products }]);
  }

  changeIsAddress() {
    this.isAddress = !this.isAddress;
  }
  changeIsShedule() {
    this.isShedule = !this.isShedule;
  }

  // изменение адреса, для начала изменений двойной клик
  renameAddress(event) {
    this.store.address = event.currentTarget.value;
    this.isAddress = !this.isAddress;
  }

  // изменение времени работы, для начала изменений двойной клик
  renameSchedule(event) {
    this.store.schedule = event.currentTarget.value;
    this.isShedule = !this.isShedule;
  }
}
