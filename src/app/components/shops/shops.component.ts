import { Component, OnInit } from '@angular/core';
import {Shop} from '../../shared/models/shop.model';
import { StorageService } from 'src/app/shared/service/storage.service';
import { ShopsService } from 'src/app/shared/service/shops.service';

@Component({
  selector: 'app-shops',
  templateUrl: './shops.component.html',
  styleUrls: ['./shops.component.less']
})

export class ShopsComponent implements OnInit {
  stores: Shop[];
  title: string;
  address: string;
  shedule: string;
  constructor(private shopsService: ShopsService,
    private storageService: StorageService) {
  }
  ngOnInit() {
    // получение всех магазинов из local storage
    this.stores = this.storageService.getLocalStorage().map((el) => {
      return new Shop(el.id, el.title, el.address, el.schedule, el.products, el.lat, el.lng);
    });
  }
  // переход на конкретный магазин используя событие
  goToShop(event) {
    this.title = event.currentTarget.innerHTML;
  }
}
