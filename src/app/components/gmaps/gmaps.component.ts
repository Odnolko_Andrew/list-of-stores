import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import { } from 'googlemaps';
import { StorageService } from 'src/app/shared/service/storage.service';
import { Shop } from 'src/app/shared/models/shop.model';

declare const google: any;

@Component({
  selector: 'app-gmaps',
  templateUrl: './gmaps.component.html',
  styleUrls: ['./gmaps.component.less']
})


export class GmapsComponent implements OnInit, AfterViewInit {
  @ViewChild('gmap') gmapElement: any;

  stores: Shop[];

  map: google.maps.Map;
  shop1: Shop;
  shop2: Shop;
  shop3: Shop;
  shop4: Shop;

  markerShop1: google.maps.Marker;
  markerShop2: google.maps.Marker;
  markerShop3: google.maps.Marker;
  markerShop4: google.maps.Marker;

  constructor(private storageService: StorageService) {}

  // получение данных с local storage
  ngAfterViewInit() {
    this.stores = this.storageService.getLocalStorage().map((el) => {
      return new Shop(el.id, el.title, el.address, el.schedule, el.products, el.lat, el.lng);
    });
    this.findShops();

    // отцентровка карты
    const mapProp = {
      center: new google.maps.LatLng(53.90722372, 27.44524261),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    // маркер первого магазина
    this.markerShop1 = new google.maps.Marker({
      position: {lat: this.shop1.lat, lng: this.shop1.lng},
      map: this.map,
      title: this.shop1.title,
      icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
      animation: google.maps.Animation.BOUNCE
    });

    // маркер второго магазина
    this.markerShop2 = new google.maps.Marker({
      position: {lat: this.shop2.lat, lng: this.shop2.lng},
      map: this.map,
      title: this.shop2.title,
      icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
      animation: google.maps.Animation.BOUNCE
    });

    // маркер третьего магазина
    this.markerShop3 = new google.maps.Marker({
      position: {lat: this.shop3.lat, lng: this.shop3.lng},
      map: this.map,
      title: this.shop3.title,
      icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
      animation: google.maps.Animation.BOUNCE
    });

    // маркер четвертого магазина
    this.markerShop4 = new google.maps.Marker({
      position: {lat: this.shop4.lat, lng: this.shop4.lng},
      map: this.map,
      title: this.shop4.title,
      icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
      animation: google.maps.Animation.BOUNCE
    });
  }
  // нахождение всех магазинов
  findShops() {
    this.stores.forEach(elem => {
      switch (elem.id) {
        case 1:
          this.shop1 = elem;
        break;
        case 2:
          this.shop2 = elem;
        break;
        case 3:
          this.shop3 = elem;
        break;
        case 4:
          this.shop4 = elem;
        break;
        }
      });
    }
  ngOnInit() {}
}

