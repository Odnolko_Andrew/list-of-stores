export class ProductModel {
  id: number;
  title: string;
  price: number;
  description: string;
  constructor(id, title, price, description) {
    this.id = id;
    this.title = title;
    this.price = price;
    this.description = description;
  }
}


