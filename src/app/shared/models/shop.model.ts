import {ProductModel} from './product.model';

export class Shop {
  id: number;
  title: string;
  address: string;
  lat: number;
  lng: number;
  schedule: string;
  products: ProductModel[];
  constructor(id, title, address, schedule, products, lat, lng) {
    this.id = id;
    this.title = title;
    this.address = address;
    this.schedule = schedule;
    this.products = products;
    this.lat = lat;
    this.lng = lng;
  }
}
