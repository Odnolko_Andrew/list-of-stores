import {Shop} from '../models/shop.model';
import {ProductModel} from '../models/product.model';

export class ShopsService {

  private shop1: Shop;
  private shop2: Shop;
  private shop3: Shop;
  private shop4: Shop;

  private products1: ProductModel[] = [];
  private products2: ProductModel[] = [];
  private products3: ProductModel[] = [];
  private products4: ProductModel[] = [];

  listShops: Shop[] = [];

  constructor() {
    // создание товаров для каждого магазина
    this.productCreate(this.products1, 1, 'product1', 3000, 'About product...');
    this.productCreate(this.products1, 2, 'product2', 2000, 'About product...');
    this.productCreate(this.products1, 3, 'product3', 1000, 'About product...');
    this.productCreate(this.products1, 4, 'product4', 4000, 'About product...');

    this.productCreate(this.products2, 1, 'product1', 5000, 'About product...');
    this.productCreate(this.products2, 2, 'product2', 6000, 'About product...');
    this.productCreate(this.products2, 3, 'product3', 7000, 'About product...');

    this.productCreate(this.products3, 1, 'product1', 7000, 'About product...');
    this.productCreate(this.products3, 2, 'product2', 2000, 'About product...');

    this.productCreate(this.products4, 1, 'product1', 7000, 'About product...');

    // создание магазинов
    this.shop1 = this.shopCreate(1, 'shop1', 'addressShop1', '12-20', this.products1, 53.90722372, 27.44524261);
    this.shop2 = this.shopCreate(2, 'shop2', 'addressShop2', '12-21', this.products2, 53.904675, 27.447372);
    this.shop3 = this.shopCreate(3, 'shop3', 'addressShop3', '12-22', this.products3, 53.90901, 27.450130);
    this.shop4 = this.shopCreate(4, 'shop4', 'addressShop4', '12-23', this.products4, 53.90101, 27.450130);

    this.listShops.push(this.shop1);
    this.listShops.push(this.shop2);
    this.listShops.push(this.shop3);
    this.listShops.push(this.shop4);
  }

  getListshops() {
    return this.listShops;
  }
  shopCreate(id, name, address, schedule, products, lat, lng) {
    return new Shop(id, name, address, schedule, products, lat, lng);
  }
  private productCreate(products, id, title, price, description) {
   return products.push(new ProductModel(id, title, price, description));
  }
}
