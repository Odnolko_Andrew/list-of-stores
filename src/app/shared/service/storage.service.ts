import {ShopsService} from './shops.service';
import {Injectable} from '@angular/core';

@Injectable()
export class StorageService {
  key = 'shops';
  constructor(private shopsService: ShopsService) {
    this.setLocalStorage(this.key, this.shopsService.getListshops());
  }

  setLocalStorage(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
  }

  getLocalStorage() {
    return JSON.parse(localStorage.getItem(this.key));
  }

}
