import {NgModule} from '@angular/core';

import {RouterModule, Routes} from '@angular/router';
import {ShopsComponent} from './components/shops/shops.component';
import { ProductsComponent } from './components/shops/shop/products/products.component';

const routes: Routes = [
  {path: '', redirectTo: 'shops', pathMatch: 'full'},
  {path: 'shops', component: ShopsComponent},
  {path: 'products', component: ProductsComponent},

];

@NgModule({
  imports: [
  RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
